/****************************************************
* Name          :  TestUtilityControllerTest
* Description   :  Test class of TestUtilityController
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
**************************************************/

@isTest
private class TestUtilityControllerTest {

    @isTest static void unitTest(){
        
        Test.startTest();
        TestUtilityController.getJobOpening('Test Tittle', 'Remote', 'Test Description');
        TestUtilityController.getAttachment('File Name Test', 'Test Body', '01p5g000009SATr');
        TestUtilityController.getContact('First Test Name', 'Last Test Name', 'testemailgmail@gmail.com');
        TestUtilityController.getJobApplication('First Name', 'Last Name', 'testemail@gmail.com', '9898989898');
        TestUtilityController.getDocument(true, '01p5g000009SATr', 'image/jpeg', Label.Job_App_Logo_Document_Name, 'Some Text');
        Test.stopTest();
    }
}