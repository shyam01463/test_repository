/***********************************************************************************
* Name          :  TestUtilityController
* Description   :  Test data helper class
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
/***********************************************************************************/

public without sharing class TestUtilityController {

    public static Job_Opening__c getJobOpening(String title, String location, String description){
        Job_Opening__c jobOpening = new Job_Opening__c(Job_Title__c = title, Location__c = location);
        jobOpening.Description__c = description;
        return jobOpening;
    }
    
    public static Job_Application__c getJobApplication(String fName, String lName, String email, String phone){
        Job_Application__c jobApp = new Job_Application__c(First_Name__c = fName, Last_Name__c = lName, Email__c = email);
        jobApp.Phone__c = phone;
        return jobApp;
    }
    
    public static Document getDocument(Boolean isPublic, String folderId, String contentType, String name, String body){
        Document doc = new Document(IsPublic = isPublic, FolderId = folderId, ContentType = contentType);
        doc.Name = name;
        doc.Body = Blob.valueOf(body);
        return doc;
    }
    
    public static Contact getContact(String fName, String lName, String email){
        Contact con = new Contact(FirstName = fName, LastName = lName, Email = email);
        return con;
    }

    public static Attachment getAttachment(String fileName, String body, String parentId){
        Attachment att = new Attachment(Name = fileName, Body = Blob.valueOf(body), ParentId = parentId);
        return att;
    }
}