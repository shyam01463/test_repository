/***********************************************************************************
* Name          :  BatchAttachmentToCVFile
* Description   :  Batch class to convert Attachmens into Content Version
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
/***********************************************************************************/
/*Set<Id> jobAppIds = new Set<Id>();
Database.executebatch(new BatchAttachmentToCVFile(jobAppIds), 1);*/

public without sharing class BatchAttachmentToCVFile implements Database.Batchable<sObject>{
 
    public Set<Id> jobAppIds;
    
    public BatchAttachmentToCVFile(){}
    
    public BatchAttachmentToCVFile(Set<Id> jobAppIdSet){
        jobAppIds = jobAppIdSet;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
       
        String queryString = 'SELECT Id,Name FROM Job_Application__c WHERE Is_Content_Version_Created__c = false';
        
        if(jobAppIds != null && !jobAppIds.isEmpty()){
            queryString += ' AND Id IN : jobAppIds';
        }
        return Database.getQueryLocator(queryString);
    }
    
    public void execute(Database.BatchableContext BC, List<Job_Application__c> scope){
    
        List<Job_Application__c> jobAppList = scope;
        convertAttachmentToContentVersion(jobAppList[0].Id);
    }
    
    public void finish(Database.BatchableContext BC){

        Boolean canSchedule = true;
        List<CronTrigger> cronTriggerLi = [SELECT Id, CronJobDetail.Name FROM CronTrigger Where CronJobDetail.Name =: Label.Job_App_Scheduler_Name];
        if(cronTriggerLi.size() > 0){
            canSchedule = false;
            System.abortJob(cronTriggerLi[0].Id);
            canSchedule = true;
        }
        if(canSchedule){
            Datetime dt = Datetime.now().addMinutes(Integer.valueOf(Label.Job_App_Schedule_Minute));
            String cronExp = '0 ' + dt.minute() + ' ' + dt.hour() + ' ' + dt.day() + ' ' + dt.month() + ' ?' + ' ' + dt.year();
            System.schedule(Label.Job_App_Scheduler_Name, cronExp, new ScheduleBatchAttachmentToCVFile());
        }
    }
    
    static void convertAttachmentToContentVersion(String jobAppId){
    
        if(String.isNotBlank(jobAppId)){
        
            List<Attachment> attachmentList = [SELECT Id,Name,Body,OwnerId,ParentId FROM Attachment WHERE ParentId =: jobAppId];
            if(!attachmentList.isEmpty()){

                List<ContentVersion> cvInsertList = new List<ContentVersion>();
                for(Attachment attachment : attachmentList){
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.VersionData = attachment.Body;
                    cv.PathOnClient = attachment.Name;
                    cv.Title = attachment.Name.contains('.') ? attachment.Name.substringBeforeLast('.') : attachment.Name;
                    
                    cvInsertList.add(cv);
                }
                
                if(!cvInsertList.isEmpty()){
                    insert cvInsertList;
            
                    List<ContentDocumentLink> cdLinkInsertList = new List<ContentDocumentLink>();
                    for(ContentVersion cv : [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id IN : cvInsertList AND ContentDocumentId != null AND IsLatest = true]){
                        ContentDocumentLink cdLink = new ContentDocumentLink();
                        cdLink.ShareType = 'V';
                        cdLink.LinkedEntityId = jobAppId;
                        cdLink.ContentDocumentId = cv.ContentDocumentId;
                        
                        cdLinkInsertList.add(cdLink);
                    }
                    
                    if(!cdLinkInsertList.isEmpty()){
                        insert cdLinkInsertList;
                    
                        if(Schema.sObjectType.Attachment.isDeletable()){
                            delete attachmentList;
                        
                            if(Schema.sObjectType.Job_Application__c.fields.Is_Content_Version_Created__c.isUpdateable()){
                                Job_Application__c jobApp = new Job_Application__c(Id = jobAppId, Is_Content_Version_Created__c = true);
                                update jobApp;
                            }
                        }
                    }
                }
            }
        }
    }
}