/***********************************************************************************
* Name          :  JobAppListingController
* Description   :  Handler class of JobAppListing LWC
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer           Comments
------------------------------------------------------------------------------------
03/25/2021      1.0.0       NLINEAXIS           Created
05/18/2021      1.1.0       NLINEAXIS           Modified:- Added new method getJobAppDetail
05/19/2021      1.2.0       NLINEAXIS           Modified:- Added new method updateJobAppDetail
05/25/2021      1.3.0       NLINEAXIS (S.V.)    Modified:- Added new wrapper class JobAppFileDataWrapper

/***********************************************************************************/
public class JobAppListingController {

    @AuraEnabled(cacheable=true)
    public static List<List<FieldSetWrapper>> getjobAppFieldSet() {
        
        List<FieldSetWrapper> fieldSetWrapperLi = new List<FieldSetWrapper>();
        List<List<FieldSetWrapper>> returnDataLi = new List<List<FieldSetWrapper>>();
        List<Schema.FieldSetMember> fieldSetMemberLi = SObjectType.Job_Application__c.FieldSets.Job_Candidates.getFields();

        if(fieldSetMemberLi.size() > 0){
            for(Schema.FieldSetMember fieldSetMember : fieldSetMemberLi) {
                FieldSetWrapper wrap = new FieldSetWrapper();
                wrap.fieldLabel = fieldSetMember.getLabel();
                wrap.fieldAPIName = fieldSetMember.getSObjectField()+'';
                fieldSetWrapperLi.add(wrap);
            }
        }
        returnDataLi.add(fieldSetWrapperLi);

        //Status__c picklist values
        fieldSetWrapperLi = new List<FieldSetWrapper>();
        Schema.DescribeFieldResult statusFieldResult = Job_Application__c.Status__c.getDescribe();

        if(statusFieldResult.isAccessible()){
            List<Schema.PicklistEntry> statusPicklistEntry = statusFieldResult.getPicklistValues();

            if(statusPicklistEntry.size() > 0){
                for(Schema.PicklistEntry pickListVal : statusPicklistEntry){
                    FieldSetWrapper wrap = new FieldSetWrapper();
                    wrap.fieldLabel = pickListVal.getLabel();
                    wrap.fieldAPIName = pickListVal.getValue();
                    fieldSetWrapperLi.add(wrap);
                }
                returnDataLi.add(fieldSetWrapperLi);
            }
        }

        return returnDataLi;
    }

    @AuraEnabled
    public static JobAppDetailData getJobAppData(String jobOpeningId, String searchString, String selectedStatus) {

        JobAppDetailData jobAppData = new JobAppDetailData();
        List<Job_Application__c> jobAppLi = new List<Job_Application__c>();
        List<Schema.FieldSetMember> fieldSetMemberLi = SObjectType.Job_Application__c.FieldSets.Job_Candidates.getFields();

        if(String.isNotBlank(jobOpeningId) && fieldSetMemberLi.size() > 0){

            String fieldNames = 'Id' + getJobAppFieldAPINames(fieldSetMemberLi);
            if(String.isNotBlank(fieldNames)){

                if(String.isNotBlank(searchString)){
                    String queryString = 'FIND \'' + searchString + '*\' RETURNING  Job_Application__c (' + fieldNames + ' WHERE Job_Opening__c =: jobOpeningId';
                    
                    if(String.isNotBlank(selectedStatus)){
                        queryString += ' AND Status__c =: selectedStatus';
                    }
                    queryString += ')';

                    List<List<sObject>> searchList = search.query(queryString);
                    if(searchList.size() > 0){
                        List<sObject> jobAppList = searchList[0];
                        if(jobAppList.size() > 0){
                            jobAppLi = jobAppList;
                        }
                    }
                }
                else{
                    jobAppData.isListedAll = true;
                    String queryString = 'Select ' + fieldNames + ' From Job_Application__c Where Job_Opening__c =: jobOpeningId';
                    
                    if(String.isNotBlank(selectedStatus)){
                        queryString += ' AND Status__c =: selectedStatus';
                    }
                    if(String.isNotBlank(queryString)){
                        jobAppLi = Database.query(queryString);
                    }
                }

                if(jobAppLi.size() > 0){

                    List<JobAppDataWrapper> jobAppDataLi = new List<JobAppDataWrapper>();
                    for(Job_Application__c jobApp : jobAppLi){

                        JobAppDataWrapper jobAppDataWrap = new JobAppDataWrapper();
                        List<FieldSetMemberWrapper> fieldSetMemberWrapperLi = new List<FieldSetMemberWrapper>();

                        for(Schema.FieldSetMember fieldSetMember : fieldSetMemberLi) {

                            String fieldName = fieldSetMember.getSObjectField()+'';
                            FieldSetMemberWrapper wrap = new FieldSetMemberWrapper();
                            
                            wrap.recordId = jobApp.Id;
                            wrap.fieldLabel = fieldSetMember.getLabel();
                            wrap.fieldType = fieldSetMember.getType()+'';
                            wrap.isRequired = fieldSetMember.getRequired();
                            wrap.isDBRequired = fieldSetMember.getDbRequired();
                            wrap.fieldAPIName = fieldSetMember.getSObjectField()+'';
                            wrap.fieldValue = jobApp.get(fieldName) != null ? jobApp.get(fieldName)+'' : '';

                            if(String.isNotBlank(wrap.fieldValue) && wrap.fieldValue.length() > 80){
                                wrap.fieldValue = wrap.fieldValue.subString(0, 80);
                            }
                            
                            if(wrap.fieldAPIName == 'Name'){
                                wrap.isNameField = true;
                                wrap.refrenceObjName = getObjectAPIName(Id.valueOf(wrap.recordId));
                            }
                            else if(wrap.fieldAPIName == 'Email__c' && fieldNames.contains('Candidate__c')){
                                Map<String, Object> mapObj = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(jobApp));
                                if(mapObj.get('Candidate__c') != null){
                                    wrap.isEmailField = true;
                                    wrap.refrenceId = mapObj.get('Candidate__c')+'';
                                    wrap.refrenceObjName = getObjectAPIName(Id.valueOf(wrap.refrenceId));
                                }
                            }
                            else if(fieldSetMember.getType() == Schema.DisplayType.Reference && fieldName.endsWith('__c') && jobApp.get(fieldName) != null){

                                wrap.isRefrenceField = true;
                                wrap.refrenceId = jobApp.get(fieldName)+'';
                                wrap.refrenceObjName = getObjectAPIName(Id.valueOf(wrap.refrenceId));
                                fieldName = fieldName.replace('__c', '__r');

                                Map<String, Object> mapObj = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(jobApp));
                                Map<String, Object> mapCand = (Map<String, Object>)mapObj.get(fieldName);
                                wrap.refrenceName = mapCand.get('Name')+'';
                            }
                            fieldSetMemberWrapperLi.add(wrap);
                        }
                        jobAppDataWrap.keyId = jobApp.Id+'';
                        jobAppDataWrap.fieldSetMemberWrapperLi = fieldSetMemberWrapperLi;
                        jobAppDataLi.add(jobAppDataWrap);
                    }
                    jobAppData.jobAppDataLi = jobAppDataLi;
                }
            }
        }
        return jobAppData;
    }

    public static String getJobAppFieldAPINames(List<Schema.FieldSetMember> fieldSetMemberLi) {

        String fieldNames = '';
        if(fieldSetMemberLi.size() > 0){
            for(Schema.FieldSetMember fieldSetMember : fieldSetMemberLi) {
                String fieldName = fieldSetMember.getSObjectField()+'';
                if(fieldSetMember.getType() == Schema.DisplayType.Reference && fieldName.endsWith('__c')){
                    fieldName += ',' + fieldName.replace('__c', '__r.Name');
                }
                fieldNames += ',' + fieldName;
            }
        }

        return fieldNames;
    }

    public static String getObjectAPIName(Id recId) {
        return recId.getSObjectType().getDescribe().getName();
    }

    @AuraEnabled
    public static JobAppFileDataWrapper getJobAppDetail(String jobAppId) {
        
        JobAppFileDataWrapper wrapper = new JobAppFileDataWrapper();
        if(String.isNotBlank(jobAppId)){

            String queryString = 'Select Id,Name,Candidate__c,Candidate__r.Name,Job_Opening__c,Job_Opening__r.Location__c,Email__c,';
            queryString += 'Phone__c,Status__c,Notes__c,Cover_Letter__c,Resume_Details__c From Job_Application__c Where Id =: jobAppId';
            
            if(String.isNotBlank(queryString)){
                List<Job_Application__c> jobAppLi = Database.query(queryString);
                if(jobAppLi.size() > 0){

                    wrapper.jobApp = jobAppLi[0];
                    Set<Id> contentDocIds = new Set<Id>();
                    List<FileDataWrapper> fileDataLi = new List<FileDataWrapper>();

                    for(ContentDocumentLink contentDocLink : [Select ContentDocumentId From ContentDocumentLink Where LinkedEntityId =: jobAppLi[0].Id]){
                        if(String.isNotBlank(contentDocLink.ContentDocumentId)){
                            contentDocIds.add(contentDocLink.ContentDocumentId);
                        }
                    }

                    if(contentDocIds.size() > 0){
                        for(ContentVersion cv : [Select Id,ContentDocumentId,Title,FileExtension,FileType,ContentSize,CreatedDate From ContentVersion Where ContentDocumentId IN : contentDocIds And IsLatest = true]){
                            FileDataWrapper wrap = new FileDataWrapper();
                            wrap.documentId = cv.ContentDocumentId;
                            wrap.documentTitle = cv.Title;
                            wrap.fileExtension = cv.FileExtension;
                            wrap.documentFileType = cv.FileType;
                            wrap.documentContentSize = getFileSize(cv.ContentSize);
                            wrap.createdDate = date.newinstance(cv.CreatedDate.year(), cv.CreatedDate.month(), cv.CreatedDate.day());
                            fileDataLi.add(wrap);
                        }
                    }
                    wrapper.fileDataLi = fileDataLi;
                }
            }
        }
        return wrapper;
    }

    public static String getFileSize(Double contentSize){

        String fileSize = '';
        if(contentSize < 1000){
            fileSize = contentSize.intValue()+'' + 'B';
        }
        else if(contentSize == 1000){
            fileSize = '1KB';
        }
        else if((contentSize / 1000) < 1024){
            fileSize = (contentSize / 1000).intValue()+'' + 'KB';
        }
        else if((contentSize / 1000) == 1024){
            fileSize = '1MB';
        }
        else if((contentSize / 1000) > 1024 && ((contentSize / 1000) / 1024) < 1024){
            fileSize = ((contentSize / 1000) / 1024).intValue()+'' + 'MB';
        }
        else if(((contentSize / 1000) / 1024) == 1024){
            fileSize = '1GB';
        }
        else if(((contentSize / 1000) / 1024) > 1024){
            fileSize = (((contentSize / 1000) / 1024) / 1024).intValue()+'' + 'GB';
        }

        return fileSize;
    }

    @AuraEnabled
    public static String updateJobAppDetail(String jobAppId, String notes, String status){

        if(String.isNotBlank(jobAppId)){
            update new Job_Application__c(Id = jobAppId, Notes__c = notes, Status__c = status);
        }

        return 'Job app updated.';
    }

    public class JobAppFileDataWrapper{

        @AuraEnabled public Job_Application__c jobApp           {get;set;}
        @AuraEnabled public List<FileDataWrapper> fileDataLi    {get;set;}

        public JobAppFileDataWrapper(){
            jobApp = new Job_Application__c();
            fileDataLi = new List<FileDataWrapper>();
        }
    }

    public class FileDataWrapper{

        @AuraEnabled public String documentId           {get;set;}
        @AuraEnabled public String documentTitle        {get;set;}
        @AuraEnabled public String fileExtension        {get;set;}
        @AuraEnabled public String documentFileType     {get;set;}
        @AuraEnabled public String documentContentSize  {get;set;}
        @AuraEnabled public Date createdDate            {get;set;}

        public FileDataWrapper(){
            documentId = documentTitle = fileExtension = documentFileType = documentContentSize = '';
            createdDate = null;
        }
    }

    public class JobAppDetailData{

        @AuraEnabled public Boolean isListedAll                     {get;set;}
        @AuraEnabled public List<JobAppDataWrapper> jobAppDataLi    {get;set;}

        public JobAppDetailData(){
            isListedAll = false;
            jobAppDataLi = new List<JobAppDataWrapper>();
        }
    }

    public class FieldSetWrapper{

        @AuraEnabled public String fieldLabel       {get;set;}
        @AuraEnabled public String fieldAPIName     {get;set;}

        public FieldSetWrapper(){
            fieldLabel = fieldAPIName = '';
        }
    }

    public class JobAppDataWrapper{

        @AuraEnabled public String keyId                                         {get;set;}
        @AuraEnabled public List<FieldSetMemberWrapper> fieldSetMemberWrapperLi  {get;set;}

        public JobAppDataWrapper(){
            keyId = '';
            fieldSetMemberWrapperLi = new List<FieldSetMemberWrapper>();
        }
    }

    public class FieldSetMemberWrapper{

        @AuraEnabled public String recordId         {get;set;}
        @AuraEnabled public String fieldType        {get;set;}
        @AuraEnabled public String fieldLabel       {get;set;}
        @AuraEnabled public String fieldValue       {get;set;}
        @AuraEnabled public String refrenceId       {get;set;}
        @AuraEnabled public String refrenceName     {get;set;}
        @AuraEnabled public String fieldAPIName     {get;set;}
        @AuraEnabled public String refrenceObjName  {get;set;}
        @AuraEnabled public Boolean isRequired      {get;set;}
        @AuraEnabled public Boolean isNameField     {get;set;}
        @AuraEnabled public Boolean isEmailField    {get;set;}
        @AuraEnabled public Boolean isDBRequired    {get;set;}
        @AuraEnabled public Boolean isRefrenceField {get;set;}

        public FieldSetMemberWrapper(){
            isRequired = isDBRequired = isRefrenceField = isNameField = isEmailField = false;
            fieldType = fieldLabel = fieldAPIName = recordId = fieldValue = refrenceId = refrenceName = refrenceObjName = '';
        }
    }
}