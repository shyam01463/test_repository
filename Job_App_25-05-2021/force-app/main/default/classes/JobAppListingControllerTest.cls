/***********************************************************************************
* Name          :  JobAppListingControllerTest
* Description   :  Test class of JobAJobAppListingController
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
04/01/2021      1.0.0       NLINEAXIS          Created
/***********************************************************************************/
@isTest
private class JobAppListingControllerTest {

    @isTest static void unitTest() {

        Job_Opening__c jobOpening = TestUtilityController.getJobOpening('Test Tittle', 'Remote', 'Test Description');
        insert jobOpening;
        
        Contact con = TestUtilityController.getContact('fName', 'lName', 'testcon@gmail.com');
        insert con;
        
        Job_Application__c jobApp = TestUtilityController.getJobApplication('astring', 'Test', 'testemailgmail@gmail.com', '9898989898');
        jobApp.Candidate__c = con.Id;
        jobApp.Status__c = 'New';
        jobApp.Job_Opening__c = jobOpening.Id;
        jobApp.Resume_Details__c = 'A creative and inventive thinker, who craves a challenge and a motivated team please';
        insert jobApp;
        
        Test.startTest();
        JobAppListingController.getjobAppFieldSet();
        JobAppListingController.getJobAppDetail(jobApp.Id);
        JobAppListingController.getJobAppData(jobOpening.Id, '', 'New');
        JobAppListingController.getJobAppData(jobOpening.Id, 'ast', 'New');
        JobAppListingController.updateJobAppDetail(jobApp.Id, '', '');
        Test.stopTest();
        
        System.assertNotEquals(jobApp.Id, con.Id);
    }
}