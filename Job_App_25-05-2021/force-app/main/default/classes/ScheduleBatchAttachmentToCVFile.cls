/***********************************************************************************
* Name          :  ScheduleBatchAttachmentToCVFile
* Description   :  Scheduler to execute BatchAttachmentToCVFile batch class
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
/***********************************************************************************/
/*
Datetime dt = Datetime.now().addMinutes(10);
String strSchedule = '0 ' + dt.minute() + ' ' + dt.hour() + ' ' + dt.day() + ' ' + dt.month() + ' ?' + ' ' + dt.year();
String cronId = System.schedule('Convert Attachment To Content Version', strSchedule, new ScheduleBatchAttachmentToCVFile());

System.schedule('Convert Attachment To Content Version 1', '0 0 * ? * * *', new ScheduleBatchAttachmentToCVFile());
System.schedule('Convert Attachment To Content Version 2', '0 10 * ? * * *', new ScheduleBatchAttachmentToCVFile());
System.schedule('Convert Attachment To Content Version 3', '0 20 * ? * * *', new ScheduleBatchAttachmentToCVFile());
System.schedule('Convert Attachment To Content Version 4', '0 30 * ? * * *', new ScheduleBatchAttachmentToCVFile());
System.schedule('Convert Attachment To Content Version 5', '0 40 * ? * * *', new ScheduleBatchAttachmentToCVFile());
System.schedule('Convert Attachment To Content Version 6', '0 50 * ? * * *', new ScheduleBatchAttachmentToCVFile());
*/
public without sharing class ScheduleBatchAttachmentToCVFile implements schedulable{

    public void execute(SchedulableContext sc){
        Set<Id> jobAppIds = new Set<Id>();
        Database.executebatch(new BatchAttachmentToCVFile(jobAppIds), 1);
    }
}