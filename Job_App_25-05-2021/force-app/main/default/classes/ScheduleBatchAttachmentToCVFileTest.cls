/****************************************************
* Name          : ScheduleBatchAttachmentToCVFileTest
* Description   :  Test class of ScheduleBatchAttachmentToCVFile
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
**************************************************/

@isTest
private class ScheduleBatchAttachmentToCVFileTest {

    @isTest static void unitTest(){
        
        Job_Opening__c jobOpening = TestUtilityController.getJobOpening('Test Tittle', 'Remote', 'Test Description');
        insert jobOpening;
        
        Job_Application__c jobApp = TestUtilityController.getJobApplication('FName Test', 'LName Test', 'testemailgmail@gmail.com', '9898989898');
        jobApp.Job_Opening__c = jobOpening.Id;
        insert jobApp;
        
        Attachment attachment = TestUtilityController.getAttachment('File Name Test', 'Test Body', jobApp.Id+'');
        insert attachment;
        
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String strSchedule = '0 ' + dt.minute() + ' ' + dt.hour() + ' ' + dt.day() + ' ' + dt.month() + ' ?' + ' ' + dt.year();
        String cronId = System.schedule('Test Convert Attachment To CV', strSchedule, new ScheduleBatchAttachmentToCVFile());
        
        BatchAttachmentToCVFile batchInstance = new BatchAttachmentToCVFile();
        Database.executebatch(new BatchAttachmentToCVFile(new Set<Id>{jobApp.Id}), 1);
        System.assertNotEquals(jobApp.Id, attachment.Id);
        Test.stopTest();
    }
}