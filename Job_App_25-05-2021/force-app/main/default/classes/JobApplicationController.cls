/***********************************************************************************
* Name          :  JobApplicationController
* Description   :  Handler class of Job_Application page
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/16/2021      1.0.0       NLINEAXIS          Created
/***********************************************************************************/
public without sharing class JobApplicationController {

    public String fileName              {get;set;}
    public String errorMessage          {get;set;}
    public String jobAppLogoURL         {get;set;}
    public String jobAppButtonCSS       {get;set;}
    public Transient Blob fileBody      {get;set;}
    public Job_Opening__c jobOpening    {get;set;}
    public Job_Application__c jobApp    {get;set;}
    public String jobApplicationPageCSS {get;set;}
    
    public String currentJobId;
    
    public JobApplicationController() {

        fileName = '';
        fileBody = null;
        errorMessage = '';
        jobAppLogoURL = '';
        jobAppButtonCSS = '';
        jobApplicationPageCSS = '';
        jobOpening = new Job_Opening__c();
        jobApp = new Job_Application__c();
        currentJobId = ApexPages.CurrentPage().getparameters().get('jobId');
        
        if(String.isNotBlank(currentJobId)){
            getJobOpeningData(currentJobId);
        }
        getLogoAndStyleDocumentData();
    }
    
    public void getJobOpeningData(String jobId){
        
        System.debug(Schema.sObjectType.Job_Opening__c.fields.Job_Title__c);
        if(String.isNotBlank(jobId) && Schema.sObjectType.Job_Opening__c.isAccessible() && Schema.sObjectType.Job_Opening__c.fields.Job_Title__c.isAccessible() && Schema.sObjectType.Job_Opening__c.fields.Location__c.isAccessible()) {
            List<Job_Opening__c> jobOpeningLi = [SELECT Id, Job_Title__c, Location__c FROM Job_Opening__c Where Id =: jobId];
            if(jobOpeningLi != null && jobOpeningLi.size() > 0){
                jobOpening = jobOpeningLi[0];
            }
        }
    }

    public void getLogoAndStyleDocumentData(){

        Set<String> documentNameSet = new Set<String>{Label.Job_App_Logo_Document_Name, Label.Job_Application_Page_Document_Name, Label.Job_App_Button_Document_Name};
        if(Schema.sObjectType.Document.isAccessible() && !documentNameSet.isEmpty()){
            
            List<Document> documentList = [Select Id,Name From Document Where Name IN : documentNameSet And Folder.Name =: Label.Job_App_Document_Folder_Name];
            if(!documentList.isEmpty()){

                String hostURL = System.URL.getSalesforceBaseURL().getHost();
                hostURL = hostURL.startsWith('https://') ? hostURL : 'https://' + hostURL;

                for(Document doc : documentList){
                    if(doc.Name == Label.Job_App_Logo_Document_Name){
                        jobAppLogoURL = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                    else if(doc.Name == Label.Job_App_Button_Document_Name){
                        jobAppButtonCSS = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                    else if(doc.Name == Label.Job_Application_Page_Document_Name){
                        jobApplicationPageCSS = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                }
            }
        }
    }
    
    public PageReference saveJobApplicationData() {
        
        Boolean isSuccess = false;
        if(fileBody != null && String.isNotBlank(fileName) && jobApp.Email__c != null && currentJobId != null && Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.Contact.fields.Email.isAccessible() && Schema.sObjectType.Job_Application__c.isCreateable()) {
               
            Boolean isCallFuture = false;
            jobApp.Job_Opening__c = currentJobId;
            List<Contact> conList = [SELECT Id,Name FROM Contact WHERE Email =: jobApp.Email__c LIMIT 1];

            if(conList != null && conList.Size() > 0 ) {
                jobApp.Candidate__c = conList[0].Id;
            }
            else {
                isCallFuture = true;
            }

            try{
                insert jobApp;
            }
            catch(Exception exp){
                errorMessage = exp.getMessage();
            }

            if(jobApp != null && jobApp.Id != null) {

                if(isCallFuture){
                    createContactAndUpdateJob(jobApp.Id);
                }
                if(Schema.sObjectType.Attachment.isCreateable() && Schema.sObjectType.Attachment.fields.Body.isCreateable() && Schema.sObjectType.Attachment.fields.Name.isCreateable()){
                    Attachment attachment = new Attachment();
                    attachment.Body = fileBody;
                    attachment.Name = fileName;
                    attachment.ParentId = jobApp.Id;

                    try{
                        insert attachment;
                        isSuccess = true;
                    }
                    catch(Exception exp){
                        errorMessage = exp.getMessage();
                    }
                }
            }
        }

        PageReference pr = Page.Job_Application;
        pr.getParameters().put('jobId', currentJobId);
        pr.getParameters().put('isSuccess', isSuccess+'');
        if(String.isNotBlank(errorMessage)){
            pr.getParameters().put('errorMessage', errorMessage);
        }
        pr.setRedirect(true);
        
        return pr;
    } 
    
    @future
    static void createContactAndUpdateJob(String jobApplicationId) {
        
        if(Schema.sObjectType.Job_Application__c.isAccessible() && Schema.sObjectType.Job_Application__c.fields.First_Name__c.isAccessible() && Schema.sObjectType.Job_Application__c.fields.Last_Name__c.isAccessible()
           && Schema.sObjectType.Job_Application__c.fields.Email__c.isAccessible() && Schema.sObjectType.Job_Application__c.fields.Phone__c.isAccessible()){
             
            List<Job_Application__c> jobAppList =  [SELECT Id,Name,First_Name__c,Last_Name__c,Email__c,Phone__c,Candidate__c FROM Job_Application__c WHERE Id =: jobApplicationId];
            if(jobAppList.Size() > 0 &&  Schema.sObjectType.Contact.isCreateable() && Schema.sObjectType.Contact.isAccessible()) {
                
                Contact con = new Contact(Candidate__c = true);
                if(Schema.sObjectType.Contact.fields.FirstName.isCreateable()) {
                    con.FirstName = jobAppList[0].First_Name__c;
                }
                if(Schema.sObjectType.Contact.fields.LastName.isCreateable()) {
                    con.LastName = jobAppList[0].Last_Name__c;
                }
                if(Schema.sObjectType.Contact.fields.Email.isCreateable()) {
                    con.Email = jobAppList[0].Email__c;
                }
                if(Schema.sObjectType.Contact.fields.Phone.isCreateable()) {
                    con.Phone = jobAppList[0].Phone__c;
                }
                insert con;
                
                if(con != null && con.Id != null) {
                    jobAppList[0].Candidate__c = con.Id;
                    update jobAppList;
                }
            }
        }
    }
    
    public PageReference redirectOnFirstPage(){
        PageReference pageRef = Page.Job_Listing;
        pageRef.setRedirect(true);
        return pageRef;
    }
}