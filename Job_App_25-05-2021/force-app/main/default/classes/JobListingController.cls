/***********************************************************************************
* Name          :  JobListingController
* Description   :  Handler class of JobListing page
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/16/2021      1.0.0       NLINEAXIS          Created
/***********************************************************************************/

public without sharing class JobListingController {

    public String jobTitle                      {get;set;}
    public String jobLocation                   {get;set;}
    public String jobOpeningId                  {get;set;}
    public String jobAppLogoURL                 {get;set;}
    public String jobAppButtonCSS               {get;set;}
    public String jobDetailpageCSS              {get;set;}
    public String jobListingPageCSS             {get;set;}
    public Job_Opening__c jobOpeningDetail      {get;set;}
    public List<Job_Opening__c> jobOpeningList  {get;set;}

    public JobListingController() {

        jobTitle = '';
        jobLocation = '';
        jobOpeningId = '';
        jobAppLogoURL = '';
        jobAppButtonCSS = '';
        jobDetailPageCSS = '';
        jobListingPageCSS = '';
        jobOpeningDetail = new Job_Opening__c();
        jobOpeningList = new List<Job_Opening__c>();
        
        String jobId = ApexPages.currentPage().getParameters().get('jobId');
        searchJobOpenings(jobId);
        getLogoAndStyleDocumentData(jobId);
    }

    public void searchJobs(){
        searchJobOpenings('');
    }

    public void searchJobOpenings(String jobId){  
        
        if(Schema.sObjectType.Job_Opening__c.isAccessible() && Schema.sObjectType.Job_Opening__c.fields.Job_Title__c.isAccessible() && Schema.sObjectType.Job_Opening__c.fields.Location__c.isAccessible()
            && Schema.sObjectType.Job_Opening__c.fields.Description__c.isAccessible()) {    
            
            String queryString = 'SELECT Id,Job_Title__c,Location__c,Description__c FROM Job_Opening__c';
            if(String.isNotBlank(jobTitle) || String.isNotBlank(jobLocation) || String.isNotBlank(jobId)){
                
                queryString += ' Where';
                
                if(String.isNotBlank(jobId)){
                    queryString += ' Id =: jobId';
                }
                if(String.isNotBlank(jobTitle)){
                    if(!queryString.endsWith('Where')){
                        queryString += ' AND';
                    }
                    queryString += ' Job_Title__c LIKE \'%' + jobTitle + '%\'';
                }
                if(String.isNotBlank(jobLocation)){
                    if(!queryString.endsWith('Where')){
                        queryString += ' AND';
                    }
                    queryString += ' Location__c LIKE \'%' + jobLocation + '%\'';
                }
            }

            List<Job_Opening__c> listOfJobOpening =  Database.query(queryString);
            if(listOfJobOpening != null && listOfJobOpening.size() > 0){
                if(String.isNotBlank(jobId)){
                    jobOpeningDetail = listOfJobOpening[0];
                }
                else{
                    jobOpeningList = listOfJobOpening;
                }
            }
        }
    }

    public void getLogoAndStyleDocumentData(String jobId){

        Set<String> documentNameSet = new Set<String>{Label.Job_App_Logo_Document_Name, Label.Job_Listing_Page_Document_Name, Label.Job_Detail_Page_Document_Name,
                                                        Label.Job_App_Button_Document_Name};
        if(Schema.sObjectType.Document.isAccessible() && !documentNameSet.isEmpty()){
            
            List<Document> documentList = [Select Id,Name From Document Where Name IN : documentNameSet And Folder.Name =: Label.Job_App_Document_Folder_Name];
            if(!documentList.isEmpty()){

                String hostURL = System.URL.getSalesforceBaseURL().getHost();
                hostURL = hostURL.startsWith('https://') ? hostURL : 'https://' + hostURL;

                for(Document doc : documentList){
                    if(doc.Name == Label.Job_App_Logo_Document_Name){
                        jobAppLogoURL = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                    else if(doc.Name == Label.Job_App_Button_Document_Name){
                        jobAppButtonCSS = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                    else if(String.isBlank(jobId) && doc.Name == Label.Job_Listing_Page_Document_Name){
                        jobListingPageCSS = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                    else if(String.isNotBlank(jobId) && doc.Name == Label.Job_Detail_Page_Document_Name){
                        jobDetailPageCSS = hostURL + Label.Job_App_Document_URL + doc.Id;
                    }
                }
            }
        }
    }

    public PageReference redirectOnJobDetailPage(){
        PageReference pr = Page.Job_Detail;
        if(String.isNotBlank(jobOpeningId)){
            pr.getParameters().put('jobId', jobOpeningId);
        }
        pr.setRedirect(true);

        return pr;
    }
    
    public PageReference redirectOnJobApplicationPage(){
        PageReference pr = Page.Job_Application;
        if(jobOpeningDetail != null && jobOpeningDetail.Id != null){
            pr.getParameters().put('jobId', jobOpeningDetail.Id);
        }
        pr.setRedirect(true);

        return pr;
    }
}