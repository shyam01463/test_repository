/****************************************************
* Name          :  Job_ListingControllerTest
* Description   :  Test class of Job_ListingController class
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
**************************************************/

@isTest
private class JobListingControllerTest {
    
    @isTest static void unitTest(){
        
        Job_Opening__c job =  TestUtilityController.getJobOpening('Test Tittle', 'Remote', 'Test Description');
        insert job;
        
        List<Document> listOfDocument = new List<Document>();
        String folderId = [SELECT Id FROM Folder WHERE Name =: Label.Job_App_Document_Folder_Name].Id;
        
        listOfDocument.add(TestUtilityController.getDocument(false, folderId, 'image/jpeg', Label.Job_App_Logo_Document_Name, 'Logo'));
        listOfDocument.add(TestUtilityController.getDocument(false, folderId, 'text/css', Label.Job_Listing_Page_Document_Name, 'job Listing'));
        listOfDocument.add(TestUtilityController.getDocument(false, folderId, 'text/css', Label.Job_Detail_Page_Document_Name, 'job Detail'));
        
        if(!listOfDocument.isEmpty()){
            insert listOfDocument;
        }
        
        Test.startTest();
        JobListingController controller = new JobListingController();
        controller.jobTitle = 'Test Tittle';
        controller.jobLocation = null;
        controller.searchJobs();
        controller.searchJobOpenings(job.Id);
        
        controller.jobTitle = '';
        controller.jobLocation = 'Remote';
        controller.searchJobs(); 
        controller.searchJobOpenings(job.Id);
        
        controller.jobOpeningId = job.Id;
        controller.redirectOnJobDetailPage();
        
        Test.setCurrentPage(Page.Job_Detail);
        ApexPages.currentPage().getParameters().put('jobId', job.Id);
        controller = new JobListingController();
        controller.redirectOnJobApplicationPage();
        
        System.assertEquals('Test Tittle', job.Job_Title__c);
        Test.stopTest();
    }
}