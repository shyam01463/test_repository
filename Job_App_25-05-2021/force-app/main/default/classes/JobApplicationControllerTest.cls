/****************************************************
* Name          :  JobApplicationControllerTest
* Description   :  Test class of JobApplicationController
* Author        :  NLINEAXIS

Modification Log
----------------
Date            Version     Developer          Comments
------------------------------------------------------------------------------------
03/18/2021      1.0.0       NLINEAXIS          Created
**************************************************/

@isTest
private class JobApplicationControllerTest {

    @isTest static void unitTest(){
        
        Job_Opening__c jobOpening = TestUtilityController.getJobOpening('Test Tittle', 'Remote', 'Test Description');
        insert jobOpening;
        
        List<Document> listOfDocument = new List<Document>();
        String folderId = [SELECT Id FROM Folder WHERE Name =: Label.Job_App_Document_Folder_Name].Id;
        
        listOfDocument.add(TestUtilityController.getDocument(true, folderId, 'image/jpeg', Label.Job_App_Logo_Document_Name, 'Some Text'));
        listOfDocument.add(TestUtilityController.getDocument(true, folderId, 'text/css', Label.Job_Application_Page_Document_Name, 'Some Text'));
       
        if(!listOfDocument.isEmpty()){
            insert listOfDocument;
        }
        
        ApexPages.CurrentPage().getparameters().put('jobId', jobOpening.Id);
        
        Test.startTest();
        JobApplicationController controller = new JobApplicationController();
        controller.fileName = 'Test File';
        controller.fileBody = Blob.valueOf('Test Body');
        controller.jobApp = TestUtilityController.getJobApplication('First Name', 'Last Name', 'testemail@gmail.com', '9898989898');
        controller.jobApp.LinkedIn_Profile_URL__c = 'testlinkedinurl.com';
        
        controller.saveJobApplicationData();
        
        Test.setCurrentPage(Page.Job_Listing);
        controller.redirectOnFirstPage();
        
        Contact con = TestUtilityController.getContact('First Test Name', 'Last Test Name', 'testemailgmail@gmail.com');
        insert con;
        
        controller.jobApp = TestUtilityController.getJobApplication('First Name Test', 'Last Name Test', 'testemailgmail@gmail.com', '9898989898');
        controller.jobApp.LinkedIn_Profile_URL__c = 'testlinkedinurl.com';
        controller.saveJobApplicationData();
        
        System.assertNotEquals(con.Id, controller.jobApp.Id);
        Test.stopTest();
    }
}