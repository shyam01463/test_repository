import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getjobAppFieldSet from '@salesforce/apex/JobAppListingController.getjobAppFieldSet';
import getJobAppData from '@salesforce/apex/JobAppListingController.getJobAppData';
import getJobAppDetail from '@salesforce/apex/JobAppListingController.getJobAppDetail';
import updateJobAppDetail from '@salesforce/apex/JobAppListingController.updateJobAppDetail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class JobAppListing extends NavigationMixin(LightningElement) {
    @api recordId;
    @track error;
    @track searchStr;
    @track isNoRecords;
    @track jobApplications = [];
    @track jobAppFieldSetFields = [];
    @track statusOptions = [];
    @track statusSelectOptions = [];
    @track fileDataLi = [];
    @track isModalOpen = false;
    @track jobAppDetail;
    @track modalNotes;

    isListedAll;
    selectedStatus;
    modalStatusSelected;

    connectedCallback(){
        getjobAppFieldSet({})
        .then(result => {
            this.jobAppFieldSetFields = result[0];
            this.statusOptions = result[1];
        })
        .catch(error => {
            this.error = JSON.stringify(error);
        });

        this.getJobApplicationData();
    }

    statusChangeHandler(event){
        this.isListedAll = false;
        this.selectedStatus = event.target.value;
        this.getJobApplicationData();
    }

    searchJobAppdata(event){
        this.searchStr = event.target.value;
        
        if(event.keyCode === 13){

            let searchString = this.searchStr.trim();
            if(searchString !== undefined && searchString !== "" && searchString.length === 1){
                this.showToast();
            }
            else if(searchString !== undefined && searchString == "" && this.isListedAll){
                console.log("Already listed all records.");
            }
            else {
                this.getJobApplicationData();
            }
        }
    }

    getJobApplicationData(){

        this.isNoRecords = true;
        this.jobApplications = [];

        getJobAppData({jobOpeningId: this.recordId, searchString: this.searchStr, selectedStatus: this.selectedStatus})
        .then(result => {
            if(result !== undefined){
                this.isListedAll = result.isListedAll;
                
                if(result.jobAppDataLi !== undefined && result.jobAppDataLi.length > 0){
                    this.isModalOpen = false;
                    this.isNoRecords = false;
                    this.jobApplications = result.jobAppDataLi;
                }
            }
        })
        .catch(error => {
            this.error = JSON.stringify(error);
        });
    }

    navigateToDetailPage(event){
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": event.target.title,
                "objectApiName": event.target.lang,
                "actionName": "view"
            },
        });
    }

    navigateToFileDetailPage(event){
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state:{
                selectedRecordId: event.target.title
            }
        });
    }

    showToast() {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: 'Please enter at least 2 characters.',
            variant: 'error',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    openModal(event){
        
        var jobApplicationId = event.target.name;

        getJobAppDetail({jobAppId: jobApplicationId})
        .then(result => {
            if(result !== undefined){

                console.log(result.jobApp);
                console.log(result.fileDataLi);

                this.isModalOpen = true;
                this.statusSelectOptions = [];
                this.jobAppDetail = result.jobApp;
                this.fileDataLi = result.fileDataLi;
                this.modalNotes = this.jobAppDetail.Notes__c;
                this.modalStatusSelected = this.jobAppDetail.Status__c;

                if(this.statusOptions != undefined && this.statusOptions.length > 0){
                    for(var index = 0; index < this.statusOptions.length; index++) {
                        let isTrue = false;
                        if(this.jobAppDetail.Status__c !== undefined && this.jobAppDetail.Status__c === this.statusOptions[index].fieldLabel){
                            isTrue = true;
                        };
                        var option = {id: this.statusOptions[index].fieldAPIName, label: this.statusOptions[index].fieldLabel, selected: isTrue};
                        this.statusSelectOptions.push(option);
                    }
                }
            }
        })
        .catch(error => {
            this.error = JSON.stringify(error);
        });
    }

    changeStatus(event){
        this.modalStatusSelected = event.target.value;
    }

    closeModal(){
        this.isModalOpen = false;
    }

    submitDetails(){

        this.modalNotes = this.template.querySelector(".notesCls").value;
        if(this.jobAppDetail.Notes__c === undefined && this.modalNotes === ""){
            this.modalNotes = undefined;
        }

        if(this.jobAppDetail.Status__c !== this.modalStatusSelected || this.jobAppDetail.Notes__c !== this.modalNotes){

            updateJobAppDetail({jobAppId: this.jobAppDetail.Id, notes: this.modalNotes, status: this.modalStatusSelected})
            .then(result => {
                if(result !== undefined){
                    console.log('Success');
                    this.getJobApplicationData();
                }
            })
            .catch(error => {
                this.error = JSON.stringify(error);
            });
        }
        else{
            this.isModalOpen = false;
        }
    }
}